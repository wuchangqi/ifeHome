//rem     
(function (doc, win) {    
var docEl = doc.documentElement,    
resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',    
recalc = function () {    
var clientWidth = docEl.clientWidth;    
if (!clientWidth) return;    
docEl.style.fontSize = 20 * (clientWidth / 640) + 'px';    
};    
if (!doc.addEventListener) return;    
win.addEventListener(resizeEvt, recalc, false);    
doc.addEventListener('DOMContentLoaded', recalc, false);    
})(document, window);

//新建xhr，获取数据
var request;
if(window.XMLHttpRequest){
request = new XMLHttpRequest();//IE7+,FireFox,Chrome,Opera,Safari...
}else{
request = new ActiveXobject("Microsoft.XMLHTTP");//IE6,IE5
}
request.open("GET", "http://dev.lanbaoo.com/api/weather/forecast?lat=0&lng=0&city&cityId=CN101270101");
request.send();
request.onreadystatechange = function() {
	if (request.readyState===4) {
		if (request.status===200) { 
			var jsonData = JSON.parse(request.responseText);
			document.getElementById("top").innerHTML=jsonData.shareTips;
			document.getElementById("apparentTmp").innerHTML=jsonData.current.apparentTmp+"°";
			document.getElementById("iconUrl").src=jsonData.current.iconUrl;
			document.getElementById("condTxt").innerHTML=jsonData.current.condTxt;
			document.getElementById("windDir").innerHTML=jsonData.current.windDir;
		    document.getElementById("phaseUrl").src=jsonData.forecastDaily[0].phaseUrl;
            document.getElementById("tideName").innerHTML=jsonData.forecastDaily[0].tideName;
			document.getElementById("phaseName").innerHTML=jsonData.forecastDaily[0].phaseName;

            //根据天数插入日期
            var  a = document.getElementById("weekend");
	        var a_childs = a.childNodes; 
	        //alert(childs[0].nodeName); 
	        if ( a_childs.length > 0) {
	        	for (var i = childs.length - 1; i >= 0 ; i--) {
		      	    a.removeChild(childs[i]); 
		        };  	    
	        };
	    	var a_data = jsonData.forecastDaily;
		    for (var i = 0; i < a_data.length; i++) {
		    	var date = new Date(a_data[i].weatherDate);
		    	var month = date.getMonth() + 1;
		    	var day = date.getDate();
		    	if (month < 10) {month = '0' + month;};
		    	if (day < 10) {day = '0' + day;};
		        $('div#weekend').append('<ul id='+i+' onclick="show(this)"><li class="triangle-down"></li><li><p>'+a_data[i].weekDay+'</p></li><li><p id="0-weekDay"> '+month+'-'+day+'</p></li><li><p id='+i+'-tmpMax></p></li><li><p id='+i+'-tmpMin></p></li></ul>');
		    }
            $('div#weekend').append('<img src="images/icon_sun_0@2x.png" style="margin-left:4rem;"><P id="sunrise"></P><img width="30rem" src="images/icon_sun_1@2x.png" style="margin-left:6rem;"><p id="sunset"></p>');
	        
	        for (var i = 0; i < 7; i++) {
			    document.getElementById(i+"-tmpMax").innerHTML=jsonData.forecastDaily[i].tmpMax+"°";
			    document.getElementById(i+"-tmpMin").innerHTML=jsonData.forecastDaily[i].tmpMin+"°";
		    }
            //初始化
		    var today = jsonData.forecastDaily[0].forecastHourly;

            //钓鱼指数
            var fish_index = jsonData.forecastDaily[0].moonPhase;
            for (var i = 0; i < fish_index; i++) {
            	$('span#fish_index').append('<img src="images/icon_star_0@2x.png" />');
            };
            if (fish_index == 5) {return;}else{
            	 for (var i = 0; i < 5 - fish_index; i++) {
            	$('span#fish_index').append('<img src="images/icon_star_1@2x.png" />');
                };
            }
            //风向判断
	        for (var i = 0; i < today.length; i++) {
	        	var wind = today[i].windDeg;
	        	var Deg;
	        	if (wind == 0 || wind == 360) {Deg = "icon_windDir_4@2x";};
	        	if (wind > 0 && wind <90) {Deg = "icon_windDir_5@2x";};
	        	if (wind == 90) {var Deg = "icon_windDir_6@2x";};
	        	if (wind > 90 && wind < 180) {Deg = "icon_windDir_7@2x";};
	        	if (wind == 180) {Deg = "icon_windDir_0@2x";};
	        	if (wind > 180 && wind < 270) {Deg = "icon_windDir_1@2x";};
	        	if (wind == 270) {Deg =="icon_windDir_2@2x";};
	        	if (wind > 270 && wind < 360) {Deg = "icon_windDir_3@2x";};

	            $('div#list').append('<ul><li><p>'+today[i].gap+'</p></li><li><p>'+today[i].tmp+'°</p><p>'+today[i].hum+'%</p></li><li><img src="images/icon_pressure@2x.png"><p>'+today[i].pres+'</p></li><li><img src="images/icon_rainfall@2x.png"><p>'+today[i].pop+'%</p></li><li><img src="images/windDir/'+Deg+'.png"><p>'+today[i].windDir+'</p></li><li><p>'+today[i].windSpd+'km/h</p><p>'+today[i].windSc+'</p></li></ul>');
	        }
            $("#0").children("li[class='triangle-down']").css('display','inline-block');
		    $("#0").css('color','rgb(44,165,244)');
            document.getElementById("sunrise").innerHTML=Data.forecastDaily[0].sunrise;	
		    document.getElementById("sunset").innerHTML=Data.forecastDaily[0].sunset;
		} else {
			alert("发生错误：" + request.status);
		}
	}
 }

var Data;
    $.ajax({
        type:'get',
        url:"http://dev.lanbaoo.com/api/weather/forecast?lat=0&lng=0&city&cityId=CN101270101",
        dataType:'json',
        //data:{cmd:'coordinates'},
        async:false,
        success:function(data){
            Data = data;
        }
});

//click事件
function show(obj) {
	    var ID = obj.id;
		for (var i = 0; i < 7; i++) {
			$("#"+i).children("li[class='triangle-down']").css('display','none');
			$("#"+i).css('color','#000');
		};
		$("#"+ID).children("li[class='triangle-down']").css('display','inline-block');
		$("#"+ID).css('color','rgb(44,165,244)');
		document.getElementById("sunrise").innerHTML=Data.forecastDaily[ID].sunrise;	
		document.getElementById("sunset").innerHTML=Data.forecastDaily[ID].sunset;
		document.getElementById("tideName").innerHTML=Data.forecastDaily[ID].tideName;
	    document.getElementById("phaseName").innerHTML=Data.forecastDaily[ID].phaseName;
	    document.getElementById("phaseUrl").src=Data.forecastDaily[ID].phaseUrl;
	    
	    //刷新钓鱼指数
	    var fish = document.getElementById("fish_index");
        var fish_childs = fish.childNodes;  
        if ( fish_childs.length > 0) {
        	for (var i = fish_childs.length - 1; i >= 0 ; i--) {
	      	    fish.removeChild(fish_childs[i]); 
	        };  	    
        };
        var fish_index = Data.forecastDaily[ID].moonPhase;
        for (var i = 0; i < fish_index; i++) {
        	$('span#fish_index').append('<img src="images/icon_star_0@2x.png" />');
        };
        if (fish_index == 5) {return;}else{
        	 for (var i = 0; i < 5 - fish_index; i++) {
        	$('span#fish_index').append('<img src="images/icon_star_1@2x.png" />');
            };
        }

	    //刷新时间列表
        var b = document.getElementById("list");
        var b_childs = b.childNodes; 
        //alert(childs[0].nodeName); 
        if ( b_childs.length > 0) {
        	for (var i = b_childs.length - 1; i >= 0 ; i--) {
	      	    b.removeChild(b_childs[i]); 
	        };  	    
        };
    	 var b_data = Data.forecastDaily[ID].forecastHourly;
	     //alert(num.length);
	     for (var i = 0; i < b_data.length; i++) {
	         $('div#list').append('<ul><li><p>'+b_data[i].gap+'</p></li><li><p>'+b_data[i].tmp+'°</p><p>'+b_data[i].hum+'%</p></li><li><img width="26rem" src="images/icon_pressure@2x.png"><p>'+b_data[i].pres+'</p></li><li><img width="30rem" src="images/icon_rainfall@2x.png"><p>'+b_data[i].pop+'%</p></li><li><img width="26rem" src="images/icon_wind_direction@2x.png"><p>'+b_data[i].windDir+'</p></li><li><p>22km/h</p><p>'+b_data[i].windSpd+'级</p></li></ul>');
	     }

	    
}
